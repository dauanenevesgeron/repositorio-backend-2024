package com.example.exercicio4;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class JogadorController {
    @RequestMapping(value = "/gerarjogador", method = RequestMethod.GET)
    public Jogador GerarJogador() {
        GeradorJogador gerador = new GeradorJogador();
        Jogador jogador = gerador.geraJogador();
        return jogador;
    }
}
