package org.example;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
public class Main {
    public static void main(String[] args){
        var context = new AnnotationConfigApplicationContext(Configuracao.class);
        Ator ator1 = context.getBean(Ator.class);
        ator1.setNome("");
        ator1.setNacionalidade("");
        ator1.setNmDeIndicacoes((short) 0);
        ator1.setElegivel(true);

        Filme filme1 = context.getBean(Filme.class);
        filme1.setNome("");
        filme1.setGenero("");
        filme1.setNmDeIndicacoes((short) 0);
        filme1.setElegivel(true);

        servico servico = context.getBean(servico.class);
        servico.adicionarIndicacao(ator1, "");
        servico.adicionarIndicacao(ator1, "");
        servico.adicionarIndicacao(ator1, "");
        servico.adicionarIndicacao(ator1, "");

        servico.adicionarIndicacao(filme1, "Filme");
        servico.adicionarIndicacao(filme1, "Filme");
        servico.adicionarIndicacao(filme1, "Filme");
        servico.mostrarListaDeIndicados();
    }

