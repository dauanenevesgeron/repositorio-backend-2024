 private Boolean elegivel = false;
    @Autowired
    public Filme(String nome, String genero, Short nmrIndicacoes, Boolean elegivel) {
        this.nome = nome;
        this.genero = genero;
        this.nmrIndicacoes = nmrIndicacoes;
        this.elegivel = elegivel;
    }
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getGenero() {
        return genero;
    }
    public void setGenero(String genero) {
        this.genero = genero;
    }
    @Override
    public Short getnmrIndicacoes() {
        return nmrIndicacoes;
    }
    @Override
    public void setnmrIndicacoes(Short nmrIndicacoess) {
        this.nmrIndicacoes = nmrIndicacoes;
    }
    @Override
    public Boolean getElegivel() {
        return elegivel;
    }
    @Override
    public void setElegivel(Boolean elegivel) {
        this.elegivel = elegivel;
    }

    @Override
    public String setString() {
        return "Nome: " + this.nome + "  " + "Indicações: " + this.nmrIndicacoes + " ";
    }
}}
